const tabItems = document.querySelector('.tabs-title-list');
const tabContents = document.querySelector('.tabs-content-list');

const items = document.querySelectorAll('.tabs-title-item');
const itemTexts = document.querySelectorAll('.tabs-content-item');

tabItems.addEventListener('click', (event) => {
    for (const item of items) {
        item.classList.remove('tabs-title-item-choose')
    }
    if (event.target.closest('li')) {
        event.target.closest('li').classList.add('tabs-title-item-choose')
    }
    items.forEach(elItem => {
        if (elItem.classList.contains('tabs-title-item-choose')) {
            itemTexts.forEach(elText => {
                if (elText.dataset.text === elItem.dataset.name) {
                    elText.style.display = 'flex';
                } else {
                    elText.style.display = 'none';
                }
            })
        }
    })

});

const btnMore = document.querySelector('.btn-image');

const workList = document.querySelector('.work-list');
const imageItems = document.getElementsByClassName('image-list-item');
const listItems = document.querySelectorAll('.work-item');
const gallery = document.querySelector('.image-list');



btnMore.addEventListener('click', addMoreImg);

function addMoreImg() {
    const addImages = [{ name: "graphicDesign1", category: "graphicDesign" }, { name: "graphicDesign2", category: "graphicDesign" }, { name: "graphicDesign3", category: "graphicDesign" },
    { name: "landingPages1", category: "landingPages" }, { name: "landingPages2", category: "landingPages" }, { name: "landingPages3", category: "landingPages" },
    { name: "webDesign1", category: "webDesign" }, { name: "webDesign2", category: "webDesign" }, { name: "webDesign3", category: "webDesign" },
    { name: "wordpress4", category: "wordpress" }, { name: "wordpress5", category: "wordpress" }, { name: "wordpress6", category: "wordpress" }];
   
    addImages.forEach(image => {
        let li = document.createElement('li');
        li.className = `image-list-item ${image.category}`;

        gallery.appendChild(li);

        let imageItem = document.createElement('img');
        imageItem.className = 'image-item';
        imageItem.setAttribute('src', './img/work/' + image.name + '.jpg');

        li.appendChild(imageItem);

        let div = document.createElement('div');
        div.className = 'image-hover';
        div.innerHTML = `<img class="image-line" src="./img/Rectangle 24 copy 5.png" alt="blue line"><div class="image-icon"><a class="image-ellipse-wrapper" href="#"><img class="image-ellipse-transparent" src="./img/Ellipse 1 copy 3.png" alt="ellipse"><img class="image-ellipse-hover" src="./img/Ellipse 1.png" alt="ellipse"><img class="image-ellipse-content" src="./img/Combined shape 7431.svg" alt="icon"></a><a class="image-ellipse-wrapper" href="#"><img class="image-ellipse-transparent" src="./img/Ellipse 1 copy 3.png" alt="ellipse"><img class="image-ellipse-hover" src="./img/Ellipse 1.png" alt="ellipse"><img class="image-ellipse-content" src="./img/Layer 23.png" alt="icon"></a></div><p class="image-item-text1">creative design</p><p class="image-item-text2">${image.category}</p>`
        li.appendChild(div);
    })

    btnMore.style.display = 'none';
}


workList.addEventListener('click', event => {
    const targetId = event.target.dataset.id;

    listItems.forEach(listItem => {
        listItem.classList.remove('active-work');
    })

    event.target.classList.add('active-work');

    switch (targetId) {
        case "all":
            getItem('image-list-item')
            break;
        case "graphicDesign":
            getItem(targetId)
            break;
        case "webDesign":
            getItem(targetId)
            break;
        case "landingPages":
            getItem(targetId)
            break;
        case "wordpress":
            getItem(targetId)
            break;
    }
})


function getItem(className) {

    for (const item of imageItems) {
        if (item.classList.contains(className)) {
            item.style.display = 'block'
        } else {
            item.style.display = 'none';
        }
    }
}



const rounbout = document.querySelector('.roundabout');

const arrowNext = document.querySelector('.arrow-next');
const arrowPrew = document.querySelector('.arrow-prew');
const iconImages = document.querySelectorAll('.roundabout-image-wrapper');
const personsContent = document.querySelectorAll('.what-people-say-text');


rounbout.addEventListener('click', (event) => {

    if (!event.target.closest('.roundabout-image-wrapper') && !event.target.closest('.arrow')) {
        return
    }

    if (event.target.closest('.roundabout-image-wrapper')) {
        for (const iconImage of iconImages) {
            iconImage.classList.remove('active');
        }
        event.target.closest('.roundabout-image-wrapper').classList.add('active')
    }


    if (event.target.closest('.arrow-next')) {

        let count;

        for (let i = 0; i < iconImages.length; i++) {
           
            if (iconImages[i].classList.contains('active')) {
                iconImages[i].classList.remove('active');
                count = i + 1;
                if (i === 3) {
                    count = 0
                }
            }
        }
        iconImages[count].classList.add('active')
    }

    if (event.target.closest('.arrow-prew')) {

        let count;

        for (let i = 0; i < iconImages.length; i++) {

            if (iconImages[i].classList.contains('active')) {
                iconImages[i].classList.remove('active');
                count = i - 1;
            }
            if (i === 0) {
                count = 3;
            }
        }
        iconImages[count].classList.add('active')
    }

    for (const image of iconImages) {
        if (image.classList.contains('active')) {
            for (const person of personsContent) {
                person.style.display = 'none'
                if (person.dataset.person === image.dataset.num) {
                    person.style.display = 'block'
                }
                
            }
        }
    }
})

